import { AppRegistry } from "react-native";
import App from "./App";

AppRegistry.registerComponent("AwesomeSample", () => App);

if (window.document) {
  AppRegistry.runApplication("AwesomeSample", {
    initialProps: {},
    rootTag: document.getElementById("react-root")
  });
}
